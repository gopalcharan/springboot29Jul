package com.mindtree.springMysql.entity;

public class Item {
	private String id;
	private String itemName;
	private int itemPrice;
	public Item() {
		super();
	}
	public Item(String id, String iName, int price) {
		super();
		this.id = id;
		this.itemName = iName;
		this.itemPrice = price;
	}
	public String getId() {
		return id;
	}
	public void setId(String itemId) {
		this.id = itemId;
	}
	public String getitemName() {
		return itemName;
	}
	public void setName(String name) {
		this.itemName = name;
	}
	public int getitemPrice() {
		return itemPrice;
	}
	public void setitemPrice(int iPrice) {
		this.itemPrice = iPrice;
	}
	@Override
	public String toString() {
		return "campusmind [id=" + id + ", name=" + itemName + ", price=" + itemPrice + "]";
	}
	

}
