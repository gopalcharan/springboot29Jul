package com.mindtree.springMysql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.mindtree.springMysql.dao.ItemDao;

@SpringBootApplication
public class SpringMysqlApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringMysqlApplication.class, args);
		ItemDao itemDaoObrj = new ItemDao();
		itemDaoObrj.getAllCampusMinds();
	}

}

