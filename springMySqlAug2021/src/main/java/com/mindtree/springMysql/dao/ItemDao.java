package com.mindtree.springMysql.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mindtree.springMysql.entity.Item;
import com.mindtree.springMysql.utility.Utility;



public class ItemDao {
	
	
	public List<Item> getAllCampusMinds() {
		ArrayList<Item> campusmindlist=new ArrayList<Item>();

		Connection con;
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			String sql = "SELECT id,name,price FROM item";
			ResultSet resultSet = statement.executeQuery(sql);
			while(resultSet.next()) {
				Item itemObj = new Item();
				itemObj.setId(resultSet.getString("id"));
				itemObj.setName(resultSet.getString("name"));
				itemObj.setitemPrice(resultSet.getInt("price"));
				campusmindlist.add(itemObj);
				//System.out.println(itemObj);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return campusmindlist;
	}
	
	public String addCampusmind(Item campusmind) {
		Connection con;
		int result = 0;
		List<String> mids = new ArrayList<String>();
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			System.out.println(campusmind.getId());
			System.out.println(campusmind.getitemName());
			System.out.println(campusmind.getitemPrice());
			String sql = "insert into item values('"+campusmind.getId()+"','"+campusmind.getitemName()+"','"+campusmind.getitemPrice()+"');";
			System.out.println(sql);
			result = statement.executeUpdate(sql);
			
			//mids.add("ABC1000034");
			//mids.add("ABC234242");
			//mids.add(campusmind.getMid());
			//mids.add("abc");
			con.close();
		} catch (SQLException e) {

		}
		return campusmind.getId();
	}
	
	public String updateCampusmindById(Item cmToBeUpdated) {
		Connection con;
		Item cm = null;
		//int res=0;
		String res = null;
		try {
			//cm = getEmployeeById(cmToBeUpdated.getMid());
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			//String sql = "update student set name='"+cmToBeUpdated.getName()+"', track='"+cmToBeUpdated.getMark()+"' where mid='"+cmToBeUpdated.getId()+"';" ;
			String sql = "update item set name='"+cmToBeUpdated.getitemName()+"', price='"+cmToBeUpdated.getitemPrice()+"' where id='"+cmToBeUpdated.getId()+"';" ;
			System.out.println(sql);
			//res = statement.executeUpdate(sql);
			res = "Record updated successfully";
			con.close();
		} catch (SQLException e) {
		}
		
		return res;
	
			/*Student s = students.get(student.getStudentId());
			s.setCourse(student.getCourse());
			s.setStudentName(student.getStudentName());
			students.put(student.getStudentId(), student);*/
		
	}
	
	public void deleteCampusmindById(String cmToBeDeleted) {
		Connection con;
		Item cm = null;
		int res=0;
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			String sql = "delete from item where id='"+cmToBeDeleted+"';" ;
			System.out.println(sql);
			res = statement.executeUpdate(sql);
			System.out.println("Record Updated Successfully");
			con.close();
		} catch (SQLException e) {
		}
		
		//return res;
	}
	
	public Item getEmployeeById(String id) {
		Connection con;
		Item cm = null;
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			String sql = "select * from item where id = "+id+";";
			ResultSet resultSet = statement.executeQuery(sql);
			if(resultSet.next()) {
				cm = new Item();
				cm.setName(resultSet.getString("name"));
				cm.setitemPrice(resultSet.getInt("price"));
				cm.setId(resultSet.getString("id"));
			}
			con.close();
		} catch (SQLException e) {
		}
		return cm;
	}

}
