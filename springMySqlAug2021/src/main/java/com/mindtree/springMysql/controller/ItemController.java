package com.mindtree.springMysql.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.mindtree.springMysql.dao.ItemDao;
import com.mindtree.springMysql.entity.Item;
@Controller
public class ItemController {
	
	

	   @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	   public String index() {
	      return "index";
	   }
		@ResponseBody
		@RequestMapping(value="/getallstudent",  method = RequestMethod.GET)
		public List<Item> getAllCampusMinds() {
			ItemDao campusmindDao = new ItemDao();
			List<Item> campusminds = campusmindDao.getAllCampusMinds();
			return campusminds;
		}
		
		@ResponseBody
		@RequestMapping(value="/addstudent", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
		public String addCampusmind(@RequestBody Item campusmind) {
			ItemDao campusmindDao = new ItemDao();
			return campusmindDao.addCampusmind(campusmind);
		}
		
		@ResponseBody
		@RequestMapping(value="/updatestudent", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
		public String updateCampusmindById(@RequestBody Item campusmind) {
			ItemDao campusmindDao = new ItemDao();
			return campusmindDao.updateCampusmindById(campusmind);
		}
		
		@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
		@ResponseBody
		public String deleteCampusMind(@PathVariable("id") String id) {
			ItemDao campusmindDao = new ItemDao();
			campusmindDao.deleteCampusmindById(id);
			return id;
		}

}
