package com.mindtree.springMysql.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.mindtree.springMysql.dao.CourseDao;
import com.mindtree.springMysql.entity.Course;
@Controller
public class CourseController {
	
	

	   @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	   public String index() {
	      return "index";
	   }
		@ResponseBody
		@RequestMapping(value="/getallcourse",  method = RequestMethod.GET)
		public List<Course> getAllCampusMinds() {
			CourseDao campusmindDao = new CourseDao();
			List<Course> campusminds = campusmindDao.getAllCampusMinds();
			return campusminds;
		}
		
		@ResponseBody
		@RequestMapping(value="/addcourse", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
		public String addCampusmind(@RequestBody Course campusmind) {
			CourseDao campusmindDao = new CourseDao();
			return campusmindDao.addCampusmind(campusmind);
		}
		
		@ResponseBody
		@RequestMapping(value="/updatecourse", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
		public String updateCampusmindById(@RequestBody Course campusmind) {
			CourseDao campusmindDao = new CourseDao();
			return campusmindDao.updateCampusmindById(campusmind);
		}
		
		@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
		@ResponseBody
		public String deleteCampusMind(@PathVariable("id") String id) {
			CourseDao campusmindDao = new CourseDao();
			campusmindDao.deleteCampusmindById(id);
			return id;
		}

}
