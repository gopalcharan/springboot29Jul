package com.mindtree.springMysql.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mindtree.springMysql.entity.Course;
import com.mindtree.springMysql.utility.Utility;



public class CourseDao {
	
	
	public List<Course> getAllCampusMinds() {
		ArrayList<Course> campusmindlist=new ArrayList<Course>();

		Connection con;
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			String sql = "SELECT cid,name,fee FROM course";
			ResultSet resultSet = statement.executeQuery(sql);
			while(resultSet.next()) {
				Course campusmind = new Course();
				campusmind.setId(resultSet.getString("cid"));
				campusmind.setName(resultSet.getString("name"));
				campusmind.setFee(resultSet.getInt("fee"));
				campusmindlist.add(campusmind);
				//System.out.println(campusmind);
			}
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return campusmindlist;
	}
	
	public String addCampusmind(Course course) {
		Connection con;
		int result = 0;
		List<String> mids = new ArrayList<String>();
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			System.out.println(course.getId());
			System.out.println(course.getName());
			System.out.println(course.getFee());
			String sql = "insert into course values('"+course.getId()+"','"+course.getName()+"','"+course.getFee()+"');";
			System.out.println(sql);
			result = statement.executeUpdate(sql);
			
			//mids.add("ABC1000034");
			//mids.add("ABC234242");
			//mids.add(campusmind.getMid());
			//mids.add("abc");
			con.close();
		} catch (SQLException e) {

		}
		return course.getId();
	}
	
	public String updateCampusmindById(Course cmToBeUpdated) {
		Connection con;
		Course cm = null;
		//int res=0;
		String res = null;
		try {
			//cm = getEmployeeById(cmToBeUpdated.getMid());
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			String sql = "update course set name='"+cmToBeUpdated.getName()+"', fee='"+cmToBeUpdated.getFee()+"' where cid='"+cmToBeUpdated.getId()+"';" ;
			//String sql = "update course set name='"+cmToBeUpdated.getName()+"', fee='"+cmToBeUpdated.getFee()+"' where mid='"+"34232"+"';" ;
			System.out.println(sql);
			statement.executeUpdate(sql);
			//res = "Record updated successfully";
			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return res;
	
			/*Student s = students.get(student.getStudentId());
			s.setCourse(student.getCourse());
			s.setStudentName(student.getStudentName());
			students.put(student.getStudentId(), student);*/
		
	}
	
	public void deleteCampusmindById(String cmToBeDeleted) {
		Connection con;
		Course cm = null;
		int res=0;
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			String sql = "delete from course where cid='"+cmToBeDeleted+"';" ;
			System.out.println(sql);
			res = statement.executeUpdate(sql);
			System.out.println("Record Updated Successfully");
			con.close();
		} catch (SQLException e) {
		}
		
		//return res;
	}
	
	public Course getEmployeeById(String cid) {
		Connection con;
		Course cm = null;
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			String sql = "select * from course where cid = "+cid+";";
			ResultSet resultSet = statement.executeQuery(sql);
			if(resultSet.next()) {
				cm = new Course();
				cm.setName(resultSet.getString("name"));
				cm.setFee(resultSet.getInt("fee"));
				cm.setId(resultSet.getString("cid"));
			}
			con.close();
		} catch (SQLException e) {
		}
		return cm;
	}

}
