package com.mindtree.springMysql.entity;

public class Course {
	private String cid;
	private String name;
	private int cfee;
	public Course() {
		super();
	}
	public Course(String id, String name, int fee) {
		super();
		this.cid = id;
		this.name = name;
		this.cfee = fee;
	}
	public String getId() {
		return cid;
	}
	public void setId(String id) {
		this.cid = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getFee() {
		return cfee;
	}
	public void setFee(int fee) {
		this.cfee = fee;
	}
	@Override
	public String toString() {
		return "campusmind [id=" + cid + ", name=" + name + ", fee=" + cfee + "]";
	}
	

}
