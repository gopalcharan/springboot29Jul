package com.mindtree.springMysql.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.mindtree.springMysql.entity.Campusmind;
import com.mindtree.springMysql.utility.Utility;



public class CampusmindDao {
	
	
	public List<Campusmind> getAllCampusMinds() {
		ArrayList<Campusmind> campusmindlist=new ArrayList<Campusmind>();

		Connection con;
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			String sql = "SELECT id,name,marks FROM student";
			ResultSet resultSet = statement.executeQuery(sql);
			while(resultSet.next()) {
				Campusmind campusmind = new Campusmind();
				campusmind.setId(resultSet.getString("id"));
				campusmind.setName(resultSet.getString("name"));
				campusmind.setMark(resultSet.getInt("marks"));
				campusmindlist.add(campusmind);
			}
			con.close();
		} catch (SQLException e) {

		}
		return campusmindlist;
	}
	
	public String addCampusmind(Campusmind campusmind) {
		Connection con;
		int result = 0;
		List<String> mids = new ArrayList<String>();
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			System.out.println(campusmind.getId());
			System.out.println(campusmind.getName());
			System.out.println(campusmind.getMark());
			String sql = "insert into student values('"+campusmind.getId()+"','"+campusmind.getName()+"','"+campusmind.getMark()+"');";
			System.out.println(sql);
			result = statement.executeUpdate(sql);
			
			//mids.add("ABC1000034");
			//mids.add("ABC234242");
			//mids.add(campusmind.getMid());
			//mids.add("abc");
			con.close();
		} catch (SQLException e) {

		}
		return campusmind.getId();
	}
	
	public String updateCampusmindById(Campusmind cmToBeUpdated) {
		Connection con;
		Campusmind cm = null;
		//int res=0;
		String res = null;
		try {
			//cm = getEmployeeById(cmToBeUpdated.getMid());
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			//String sql = "update student set name='"+cmToBeUpdated.getName()+"', track='"+cmToBeUpdated.getMark()+"' where mid='"+cmToBeUpdated.getId()+"';" ;
			String sql = "update student set name='"+cmToBeUpdated.getName()+"', marks='"+cmToBeUpdated.getMark()+"' where mid='"+"34232"+"';" ;
			System.out.println(sql);
			//res = statement.executeUpdate(sql);
			res = "Record updated successfully";
			con.close();
		} catch (SQLException e) {
		}
		
		return res;
	
			/*Student s = students.get(student.getStudentId());
			s.setCourse(student.getCourse());
			s.setStudentName(student.getStudentName());
			students.put(student.getStudentId(), student);*/
		
	}
	
	public void deleteCampusmindById(String cmToBeDeleted) {
		Connection con;
		Campusmind cm = null;
		int res=0;
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			String sql = "delete from student where id='"+cmToBeDeleted+"';" ;
			System.out.println(sql);
			res = statement.executeUpdate(sql);
			System.out.println("Record Updated Successfully");
			con.close();
		} catch (SQLException e) {
		}
		
		//return res;
	}
	
	public Campusmind getEmployeeById(String mid) {
		Connection con;
		Campusmind cm = null;
		try {
			con = new Utility().getConnection();
			Statement statement = con.createStatement();
			String sql = "select * from student where id = "+mid+";";
			ResultSet resultSet = statement.executeQuery(sql);
			if(resultSet.next()) {
				cm = new Campusmind();
				cm.setName(resultSet.getString("name"));
				cm.setMark(resultSet.getInt("track"));
				cm.setId(resultSet.getString("id"));
			}
			con.close();
		} catch (SQLException e) {
		}
		return cm;
	}

}
