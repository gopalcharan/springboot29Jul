package com.mindtree.springMysql.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.mindtree.springMysql.dao.CampusmindDao;
import com.mindtree.springMysql.entity.Campusmind;
@Controller
public class CampusmindController {
	
	

	   @RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	   public String index() {
	      return "index";
	   }
		@ResponseBody
		@RequestMapping(value="/getallstudent",  method = RequestMethod.GET)
		public List<Campusmind> getAllCampusMinds() {
			CampusmindDao campusmindDao = new CampusmindDao();
			List<Campusmind> campusminds = campusmindDao.getAllCampusMinds();
			return campusminds;
		}
		
		@ResponseBody
		@RequestMapping(value="/addstudent", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
		public String addCampusmind(@RequestBody Campusmind campusmind) {
			CampusmindDao campusmindDao = new CampusmindDao();
			return campusmindDao.addCampusmind(campusmind);
		}
		
		@ResponseBody
		@RequestMapping(value="/updatestudent", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
		public String updateCampusmindById(@RequestBody Campusmind campusmind) {
			CampusmindDao campusmindDao = new CampusmindDao();
			return campusmindDao.updateCampusmindById(campusmind);
		}
		
		@RequestMapping(value="/delete/{id}", method=RequestMethod.DELETE)
		@ResponseBody
		public String deleteCampusMind(@PathVariable("id") String id) {
			CampusmindDao campusmindDao = new CampusmindDao();
			campusmindDao.deleteCampusmindById(id);
			return id;
		}

}
