package com.mindtree.springMysql.entity;

public class Campusmind {
	private String id;
	private String name;
	private int mark;
	public Campusmind() {
		super();
	}
	public Campusmind(String id, String name, int mark) {
		super();
		this.id = id;
		this.name = name;
		this.mark = mark;
	}
	public String getId() {
		return id;
	}
	public void setId(String mid) {
		this.id = mid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMark() {
		return mark;
	}
	public void setMark(int mark) {
		this.mark = mark;
	}
	@Override
	public String toString() {
		return "campusmind [id=" + id + ", name=" + name + ", marks=" + mark + "]";
	}
	

}
